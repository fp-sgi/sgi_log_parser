module SgiLogParser
  class EventQuery
    include Enumerable

    def initialize(original_events)
      @original_events = original_events
    end

    def each
      return enum_for(:each) unless block_given?

      @original_events.each do |original_event|
        event = original_event.clone
        # do something, depending on filters, etc.
        yield event
      end
    end

  end
end
