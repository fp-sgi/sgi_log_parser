module SgiLogParser
  class GameLogicError < StandardError
    def initialize(msg = 'Log entry can not be applied to current game stage')
      super
    end
  end
end
