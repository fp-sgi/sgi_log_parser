class SgiLogParser::Analysis::Level < Struct.new :parser, :counter, :scene, :start_time, :end_time
  def duration
    (self.end_time - self.start_time).round(4)
  end

  def to_s
    "Level #{counter} (Scene #{scene}), #{duration} sec."
  end

  def events
    @events ||= parser.events.select{ |e| e.state.scene == scene }
  end

  def level_id
    events.first.level_id
  end

  def level_number
    level_id[0...-1]
  end

  def level_condition
    level_id[-1, 1]
  end

  def self.find(parser)
    events = parser.events.select{ |e|
      ['LevelStart', 'LevelTimeOver', 'LevelEnd'].include? e.name
    }.group_by{ |e| e.params.counter }

    levels = events.map{ |counter, events|
      events = events.first(2)
      SgiLogParser::Analysis::Level.new(
        parser,
        counter,
        events.first.params.scene,
        events.first.time,
        events.last.time
      )
    }
  end
end
