module SgiLogParser
  class ZoneEntrance < Event

    handle 'ZoneEntrance'

    def validate
      raise(GameLogicError, 'Level not started')       unless state.scene
      raise(GameLogicError, '"player" param missing')  unless params.player
      raise(GameLogicError, '"block" param missing')   unless params.block
      raise(GameLogicError, '"element" param missing') unless params.element
    end

    def apply
    end

  end
end
