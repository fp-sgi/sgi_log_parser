module SgiLogParser
  class MovementEnd < Event

    handle 'MovementEnd'

    def validate
      raise(GameLogicError, 'Game paused')               if     state.paused
      raise(GameLogicError, 'Level not started')         unless state.scene
      raise(GameLogicError, '"player" param missing')    unless params.player
      raise(GameLogicError, '"block" param missing')     unless params.block
    end

    def apply
      case params.player
      when '1'
        raise(GameLogicError, 'player 1 not moving')     unless state.player1_direction
        state.player1_direction = nil
      when '2'
        raise(GameLogicError, 'player 2 not moving')     unless state.player2_direction
        state.player2_direction = nil
      else
        raise GameLogicError, 'player parameter invalid'
      end
    end

  end
end
