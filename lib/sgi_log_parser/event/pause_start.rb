module SgiLogParser
  class PauseStart < Event

    handle 'PauseStart'

    def validate
      raise(GameLogicError, 'Game already paused') if     state.paused
      raise(GameLogicError, 'Level not started')   unless state.scene
    end

    def apply
      state.paused = true
    end

  end
end
