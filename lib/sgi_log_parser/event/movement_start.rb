module SgiLogParser
  class MovementStart < Event

    handle 'MovementStart'

    def validate
      raise(GameLogicError, 'Game paused')               if     state.paused
      raise(GameLogicError, 'Level not started')         unless state.scene
      raise(GameLogicError, '"player" param missing')    unless params.player
      raise(GameLogicError, '"block" param missing')     unless params.block
      raise(GameLogicError, '"direction" param missing') unless params.direction
    end

    def apply
      case params.player
      when '1'
        state.player1_direction = params.direction
      when '2'
        state.player2_direction = params.direction
      else
        raise GameLogicError, 'player parameter invalid'
      end
    end

  end
end
