module SgiLogParser
  class LevelStart < Event

    handle 'LevelStart'

    def validate
      raise(GameLogicError, 'Session not started')   unless state.session
      raise(GameLogicError, 'Level already started') if     state.scene
      raise(GameLogicError, '"scene" param missing') unless params.scene
    end

    def apply
      state.scene = params.scene
    end

  end
end
