module SgiLogParser
  class LevelEnd < Event

    handle 'LevelEnd'

    def validate
      raise(GameLogicError, 'Level not started') unless state.scene
    end

    def apply
      # There are no explicit end events
      state.player1_block = nil
      state.player2_block = nil

      state.scene         = nil
    end

  end
end
