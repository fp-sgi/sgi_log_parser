module SgiLogParser
  class SessionStart < Event

    handle 'SessionStart'

    def validate
      raise(GameLogicError, 'Session already started')       if     state.session
      raise(GameLogicError, '"session" parameter missing')   unless params.session
    end

    def apply
      state.session = params.session
      state.paused  = false
    end

  end
end
