module SgiLogParser
  class Punishment < Event

    handle 'Punishment'

    def validate
      raise(GameLogicError, 'Level not started')       unless state.scene
      raise(GameLogicError, '"player" param missing')  unless params.player
      raise(GameLogicError, '"block" param missing')   unless params.block
    end

    def apply
    end

  end
end
