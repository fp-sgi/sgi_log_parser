module SgiLogParser
  class LevelTimeOver < Event

    handle 'LevelTimeOver'

    def validate
      raise(GameLogicError, 'Level not started') unless state.scene
    end

    def apply
    end

  end
end
