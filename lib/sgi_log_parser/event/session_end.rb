module SgiLogParser
  class SessionEnd < Event
    handle 'SessionEnd'

    def validate
      raise(GameLogicError, 'Session not started') unless state.session
      raise(GameLogicError, 'Level not ended')     if     state.scene
    end

    def apply
    end

  end
end
