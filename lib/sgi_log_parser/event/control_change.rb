module SgiLogParser
  class ControlChange < Event

    handle 'ControlChange'

    def validate
      raise(GameLogicError, 'Level not started')      unless state.scene
      raise(GameLogicError, '"player" param missing') unless params.player
      raise(GameLogicError, '"block" param missing')  unless params.block
    end

    def apply
      case params.player
      when '1'
        state.player1_block = params.block
      when '2'
        state.player2_block = params.block
      else
        raise GameLogicError, 'player parameter invalid'
      end
    end

  end
end
