module SgiLogParser
  class PauseEnd < Event

    handle 'PauseEnd'

    def validate
      raise(GameLogicError, 'Session not started') unless state.session
      raise(GameLogicError, 'Game not paused')     unless state.paused
    end

    def apply
      state.paused = false
    end

  end
end
