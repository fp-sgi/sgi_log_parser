module SgiLogParser
  class GoalReached < Event

    handle 'GoalReached'

    def validate
      raise(GameLogicError, 'Game paused')               if     state.paused
      raise(GameLogicError, 'Level not started')         unless state.scene
      raise(GameLogicError, '"player" param missing')    unless params.player
      raise(GameLogicError, '"block" param missing')     unless params.block
    end

    def apply
    end

  end
end
