module SgiLogParser
  GameState = Struct.new :counter, :time, :duration,
                         :session, :treatment,
                         :paused,
                         :scene,
                         :player1_block, :player2_block,
                         :player1_direction, :player2_direction
end
