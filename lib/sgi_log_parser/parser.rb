require 'json'
require 'ostruct'

module SgiLogParser
  class Parser
    attr_reader :game_state, :error_count

    def initialize(debug: false)
      @debug       = debug
      @events      = []
      @error_count = 0
      @game_state  = GameState.new
    end

    def read(file)
      file.each_line { |line| process line }
      self
    end

    def self.open(filename, debug: false)
      parser   = Parser.new debug: debug
      File.open(filename, 'r') { |f| parser.read f }
      parser
    end

    def events
      EventQuery.new @events
    end

  private

    def process(line)
      return if line.strip == '' || line.strip[0] == '#'
      time, name, *params = line.split(',', 3)
      event = EventFactory.create time:   time.strip.to_f,
                                  name:   name.strip,
                                  params: prepare_params(params.join(',').strip),
                                  previous_state: @game_state
      @events << event
      @game_state = event.process
      Event.print_success(event) if @debug
    rescue GameLogicError => e
      @error_count += 1
      Event.print_failure(event, e.message) if @debug
    end

    def prepare_params(string)
      OpenStruct.new JSON.parse(string)
    end
  end
end
