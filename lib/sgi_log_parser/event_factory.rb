module SgiLogParser
  class EventFactory
    class << self

      def register(event, object)
        @@handlers ||= {}
        @@handlers[event] = object
      end

      def create(time:, name:, params:, previous_state:)
        event_klass = @@handlers[name] || raise(GameLogicError, "Unknown event #{name}")
        return event_klass.new time: time, name: name, params: params,
                               previous_state: previous_state
      end

      def supported_events
        @@handlers.keys.sort
      end
    end
  end
end
