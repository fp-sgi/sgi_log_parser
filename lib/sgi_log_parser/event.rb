module SgiLogParser
  class Event
    attr_accessor :time, :name, :params, :state

    def initialize(time:, name:, params:, previous_state:)
      if previous_state.time
        previous_state.duration = time - previous_state.time
      end

      self.time, self.name, self.params = time, name, params

      self.state = previous_state.clone
      self.state.counter  = (self.state.counter || 0) + 1
      self.state.time     = self.time
      self.state.duration = 0
    end

    def to_s
      "#{time} #{id} #{name.bold} #{params.to_h.to_json}"
    end

    def id
      throw '"state.session" missing' unless state.session
      throw '"state.counter" missing' unless state.counter
      "#{state.session.to_s.rjust(4, '0')}-#{state.counter.to_s.rjust(8, '0')}"
    end

    def level_id
      SCENES[state.scene]
    end

    def process
      validate
      apply
      state
    end

    def validate
      throw 'Abstract event class, "validate" not implemented'
    end

    def apply
      throw 'Abstract event class, "apply" not implemented'
    end

    def self.handle(event_name)
      EventFactory.register event_name, self
    end

    def self.print_success(event)
      puts '✓'.green + " #{event}"
    end

    def self.print_failure(event, message)
      $stderr.puts '✗'.red + " #{event || 'Missing event'}\n  #{message}"
    end

  end
end
