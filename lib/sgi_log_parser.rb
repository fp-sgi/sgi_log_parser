require_relative 'sgi_log_parser/version'

require 'colorize'

module SgiLogParser
  VALID_PLAYERS = ['1', '2']

  SCENES = {
    '1_Tutorial_Goals'       => 'T1C',
    '2_Tutorial_Spikes'      => 'T2C',
    '3_Tutorial_Trampoline'  => 'T3C',
    '4_Tutorial_WaterWall'   => 'T4C',
    '5_Tutorial_Laser'       => 'T5C',
    '1_Level_Coupling_Loose' => 'A1C',
    '1_Level_Coupling_Tight' => 'A1T',
    '2_Level_Coupling_Loose' => 'A2C',
    '2_Level_Coupling_Tight' => 'A2T',
    '3_Level_Countdown_noCD' => 'B1C',
    '3_Level_Countdown_CD'   => 'B1T',
    '4_Level_Countdown_noCD' => 'B2C',
    '4_Level_Countdown_CD'   => 'B2T',
    '5_Level_Punishment_Individual' => 'C1C',
    '5_Level_Punishment_All'        => 'C1T',
    '6_Level_Punishment_Individual' => 'C2C',
    '6_Level_Punishment_All'        => 'C2T',
    '7_Level_Player_Selection_Individual' => 'D1C',
    '7_Level_Player_Selection_All'        => 'D1T',
    '8_Level_Player_Selection_Individual' => 'D2C',
    '8_Level_Player_Selection_All'        => 'D2T',
  }

  module Analysis
  end
end

require_relative 'sgi_log_parser/game_logic_error'
require_relative 'sgi_log_parser/game_state'

require_relative 'sgi_log_parser/event_factory'
require_relative 'sgi_log_parser/event'
require_relative 'sgi_log_parser/event_query'

Dir[File.dirname(__FILE__) + '/sgi_log_parser/event/*.rb'].each { |file|
  require_relative file
}

require_relative 'sgi_log_parser/parser'

require_relative 'sgi_log_parser/analysis/level'
