using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;

class Logger
  public void SessionStart(String session) {}
  public void SessionEnd() {}

  public void PlayerRegistration(String player, String code) {}
  // BlockRegistration() {} ?

  public void LevelStart(String scene) {}
  public void LevelEnd(String scene) {}

  public void LevelSkipped() {}

  // Don't forget to call when level starts
  public void BlockControlStart(String player, String block) {}
  // maybe include an end event unless that's too hard to implement

  public void PauseStart(String player) {}
  public void PauseEnd(String player) {}

  public void ZoneEntrance(String player, String block, String element) {}
  public void ZoneQuitting(String player, String block, String element) {}

  // This refers to in-game buttons in the scene
  public void ButtonPressStart(String element) {}
  public void ButtonPressEnd(String element) {}

  public void GroundedStart(String block) {}
  public void GroundedEnd(String block) {}

  public void SwimmingStart(String block) {}
  public void SwimmingEnd(String block) {}

  public void MovementStart(String player, String block, String direction) {}
  public void MovementEnd(String player, String block) {}

  public void JumpCommand(String player) {}

  public void TrampolinJump(String player, String block) {}

  // cause: readable description, element: game element
  public void BlockReset(String block, String cause, String element) {}

  public void Punishment(String player, String block) {}
end
