# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sgi_log_parser/version'

Gem::Specification.new do |spec|
  spec.name          = 'sgi_log_parser'
  spec.version       = SgiLogParser::VERSION
  spec.authors       = ['Ralf Berger']
  spec.email         = ['ralf.berger@uni-due.de']
  spec.licenses      = ['Nonstandard']

  spec.summary       = %q{Parses SGI game logs}
  # spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = 'http://fp-sgi.de/'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency     'colorize', '~> 0.8.1'
  spec.add_development_dependency 'minitest', '~> 5.9', '>= 5.9.0'
  spec.add_development_dependency 'simplecov', '~> 0'
  spec.add_development_dependency 'bundler',  '~> 1.12'
  spec.add_development_dependency 'rake',     '~> 10.0'
end
