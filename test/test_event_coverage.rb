#!/usr/bin/env ruby
require_relative 'helper'

class TestEventCoverage < Minitest::Test
  def setup
  end

  def test_all_events_covered
    events = [
      'SessionStart',  'SessionEnd',
      'LevelStart',    'LevelEnd',
      'LevelTimeOver',
      # 'ScreenStart',   'ScreenEnd',
      # 'LevelSkipped',
      'ControlChange',
      'PauseStart',    'PauseEnd',
      'ZoneEntrance',  'ZoneQuitting',
      'GoalReached',
      # 'GoalLeft',
      # 'GroundedStart', 'GroundedEnd',
      # 'SwimmingStart', 'SwimmingEnd',
      'MovementStart', 'MovementEnd',
      'JumpCommand',
      'TrampolinJump',
      'ButtonPressed',
      'BlockReset',
      'Punishment',
    ]
    events.each do |event_name|
      assert_includes SgiLogParser::EventFactory.supported_events, event_name
    end
  end
end
