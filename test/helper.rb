require 'bundler'
Bundler.setup

require 'simplecov'
SimpleCov.start

require_relative '../lib/sgi_log_parser'
require 'minitest/autorun'
