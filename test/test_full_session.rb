#!/usr/bin/env ruby
require_relative 'helper'

class TestFullSession < Minitest::Test
  def setup
    filename = "#{__dir__}/../examples/full_session.log"
    File.open(filename, 'r') do |file|
      parser   = SgiLogParser::Parser.new.read file
      @state   = parser.game_state
    end
  end

  def test_session_ended
    assert_equal '14', @state.session
  end

  def test_level_ended
    assert_equal nil, @state.scene
  end

  def test_player_blocks_unset
    assert_equal nil, @state.player1_block
    assert_equal nil, @state.player2_block
  end

  def test_player_movement_unset
    assert_equal nil, @state.player1_direction
    assert_equal nil, @state.player2_direction
  end

  def test_paused_unset
    assert_equal false, @state.paused
  end
end
