#!/usr/bin/env ruby
require_relative 'helper'

class TestOnlyStartEvents < Minitest::Test
  def setup
    filename = "#{__dir__}/../examples/only_start_events.log"
    File.open(filename, 'r') do |file|
      parser   = SgiLogParser::Parser.new.read file
      @state   = parser.game_state
    end
  end

  def test_session_started
    assert_equal '14', @state.session
  end

  def test_level_started
    assert_equal 'Level 1', @state.scene
  end

  def test_player_blocks_set
    assert_equal 'Fritzi', @state.player1_block
    assert_equal 'Hansi',  @state.player2_block
  end

  def test_player_movement_set
    assert_equal 'left',  @state.player1_direction
    assert_equal 'right', @state.player2_direction
  end

  def test_paused_set
    assert_equal true, @state.paused
  end
end
