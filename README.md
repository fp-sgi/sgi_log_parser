# sgi_log_parser

[![build status](https://gitlab.interactivesystems.info/fp_social_game_interaction/Logging/badges/master/build.svg)](https://gitlab.interactivesystems.info/fp_social_game_interaction/Logging/commits/master)
[![coverage report](https://gitlab.interactivesystems.info/fp_social_game_interaction/Logging/badges/master/coverage.svg)](https://gitlab.interactivesystems.info/fp_social_game_interaction/Logging/commits/master)

The sgi_log_parser Gem is used to read log files created by the game. See [packagecloud.io](https://packagecloud.io/fp-sgi/fp-sgi) for published versions.


## Installation via Rubygems

```sh
gem install sgi_log_parser -s https://packagecloud.io/fp-sgi/fp-sgi
```


## Installation using Bundler

Add the following lines to your `Gemfile` and run `bundle`:

```ruby
source 'https://packagecloud.io/fp-sgi/fp-sgi' do
  gem 'sgi_log_parser'
end
```


## Development setup

* Checkout the repository
* Install the requirements: `bundle`
* To run the example binary: `./bin/debug examples/full_session.csj`
* To run an interactive console: `./bin/console examples/full_session.csj`
* To run the tests: `rake test`


## Example log files

The `examples` directory contains example log file contents for a game session.


## Log file format

Log files are saved as CSJ (Comma Separated JSON). Each cell is a separate JSON object. Example:

```
# timestamp, event, parameters
1467132424.0, SessionStart, {"session": "14"}
```
